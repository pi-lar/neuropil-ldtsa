# neuropil ldtsa

The neuropil ldtsa project defines the required data structures for distributed governance.

The main concept/idea is looking at the circle of required activities that are usually part
of our daily work: separation of duties in terms of legal, executive, investigative and
juridical responsibilities is a common principle, that is often neglected when building IT 
solutions. Adding these different capabilities in later stages of projects adds significant 
costs and engineering effort. Hence this project is looking to close this gap as much as
possible, so that it is easy to "plug and play" separation of duties to projects as they 
evolve.

In this gitlab repository we would like to document and discuss the ideas introduced here:
https://www.neuropil.org/tutorial/ngiassure_ldts.html and use the fairly complex use cases
as to derive a baseline for the neuropil ldtsa project.

Our current main tasks:
- document the use cases and their rules in detail and find / define rules attributes that are
needed to accomplish the described goal (define the must/should/may ...)
- document the data structures that need to be exchanged between the involved parties
- document the interaction flows between all parties and define their protection level in terms 
of information security (confidentiality / integrity / availability / non-repudiation / ...)

Our current minor tasks:
- maintain the files in a format that allows us to 
- automatically build a documentation (gitbook?) from the files

We may find that the selected use cases are insufficient, or it could be that our described use
cases are contradicting to each other.

Ideally, we will be able to draft an RfC document (at least it should be in the shape of an RfC).


# table of contents (Proposal)

## General Information ([this page](./README.md))
## Use Case Overview / Analysis
### Remote Attestation Login ([usecases/remote_whois.md](./usecases/remote_whois.md))
### Remote Device Attestation / Intrusion Detection ([usecases/remote_whois.md](./usecases/remote_whois.md))
### Distributed Search Governance ([usecases/remote_whois.md](./usecases/remote_whois.md))
### Collecting requirements
## Security Goals / Information Security
### Integrity
### Confidentiality
### Availability
### Transparency
### Privacy
## Architectural Overview
### Data Structures
#### MerkleTree
#### SkipList
#### Data-Dependant Hashing
#### Attestation
#### Conclusion
### Algorithms
#### Accumulators
#### Hash Chaining
#### Conclusion
### Required Participants / Roles
#### Legal
#### Executive
#### Investigative
#### Juridicial
### Required Dataflows
#### L2E
#### E2I
#### I2J
#### J2L
## Literature ([./literature/literature.md](./literature/literature.md))
